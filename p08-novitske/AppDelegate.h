//
//  AppDelegate.h
//  p08-novitske
//
//  Created by Steven Novitske on 5/2/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

