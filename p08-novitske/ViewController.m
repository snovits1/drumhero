//
//  ViewController.m
//  p08-novitske
//
//  Created by Steven Novitske on 5/2/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()
@end

int screenWidth;
int screenHeight;
UIButton* snare;
UIButton* hihat;
UIButton* tomtom;
UIButton* cymbal;
UIButton* menuButton;
UIView* hitZone;
CADisplayLink* display;
UIView* notes[10][4];
UIView* menuShade;
UIView* gameOver;
UILabel* gameOverLabel;
UILabel* streakLabel;
UIView* redShade;
UISegmentedControl* levels;
UILabel* countdown;
UILabel* scoreLabel;
int score; //Total score
int longestStreak; //Longest streak of one game
int streak; //Number of hits in a row
int safe; //Decreases 1 for every mishit, increases 1 up to 40 for every hit. Once 0, you lose.
int mode; //0 for free play, 1 for random play, 2 for song play
long counter; //Counter for note reveal timing
long level; //0 for easy, 1 for medium, 2 for hard
int noteSpeed;
AVAudioPlayer* snareSound;
AVAudioPlayer* hihatSound;
AVAudioPlayer* tomtomSound;
AVAudioPlayer* cymbalSound;
AVAudioPlayer* bulls;
int* song;
int intro[71] = {2,1,35,5,7,1,14,1,35,5,7,1,35,1,14,5,7,1,35,5,7,5,14,1,35,5,1,7,1,35,1,14,5,7,1,35,5,7,5,14,1,35,5,1,7,1,35,1,14,5,7,1,35,5,7,5,14,1,35,5,1,7,1,35,1,14,5,7,1,1,15};
int bridge[17] = {35,1,35,1,10,2,35,1,35,1,10,2,10,2,10,2,1};
int verse[44] = {35,1,35,1,10,2,35,1,35,1,3,5,6,5,3,1,15,1,15,1,6,1,1,3,1,3,5,3,5,6,5,3,5,15,1,15,1,6,1,1,3,1,3,5};
int idx = 0;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    screenWidth = self.view.frame.size.width;
    screenHeight = self.view.frame.size.height;
    noteSpeed = screenHeight*0.01;
    song = intro;
    
    //Background color gradient
    CAGradientLayer* gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:118/255.0 green:124/255.0 blue:132/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithWhite:1 alpha:1].CGColor];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    //Set up red shade to indicate failure
    redShade = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    redShade.backgroundColor = [UIColor colorWithRed:192/255.0 green:0 blue:0 alpha:0];
    [self.view addSubview:redShade];
    
    //Set up snare drum button
    snare = [[UIButton alloc] initWithFrame:CGRectMake(0, screenHeight*0.85, screenWidth*0.25, screenHeight*0.15)];
    snare.backgroundColor = [UIColor redColor];
    [snare addTarget:self action:@selector(snareHit) forControlEvents:UIControlEventTouchDown];
    [snare addTarget:self action:@selector(snareRelease1) forControlEvents:UIControlEventTouchUpInside];
    [snare addTarget:self action:@selector(snareRelease2) forControlEvents:UIControlEventTouchUpOutside];
    
    //Set up hihat button
    hihat = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth*0.25, screenHeight*0.85, screenWidth*0.25, screenHeight*0.15)];
    hihat.backgroundColor = [UIColor colorWithRed:244/255.0 green:221/255.0 blue:44/255.0 alpha:1];
    [hihat addTarget:self action:@selector(hihatHit) forControlEvents:UIControlEventTouchDown];
    [hihat addTarget:self action:@selector(hihatRelease1) forControlEvents:UIControlEventTouchUpInside];
    [hihat addTarget:self action:@selector(hihatRelease2) forControlEvents:UIControlEventTouchUpOutside];
    
    //Set up tom-tom button
    tomtom = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth*0.5, screenHeight*0.85, screenWidth*0.25, screenHeight*0.15)];
    tomtom.backgroundColor = [UIColor blueColor];
    [tomtom addTarget:self action:@selector(tomtomHit) forControlEvents:UIControlEventTouchDown];
    [tomtom addTarget:self action:@selector(tomtomRelease1) forControlEvents:UIControlEventTouchUpInside];
    [tomtom addTarget:self action:@selector(tomtomRelease2) forControlEvents:UIControlEventTouchUpOutside];
    
    //Set up cymbal button
    cymbal = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth*0.75, screenHeight*0.85, screenWidth*0.25, screenHeight*0.15)];
    cymbal.backgroundColor = [UIColor colorWithRed:51/255.0 green:204/255.0 blue:71/255.0 alpha:1];
    [cymbal addTarget:self action:@selector(cymbalHit) forControlEvents:UIControlEventTouchDown];
    [cymbal addTarget:self action:@selector(cymbalRelease1) forControlEvents:UIControlEventTouchUpInside];
    [cymbal addTarget:self action:@selector(cymbalRelease2) forControlEvents:UIControlEventTouchUpOutside];
    
    //Set up menu button
    menuButton = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth*0.80, 5, screenWidth*0.20-5, screenHeight*0.05)];
    menuButton.backgroundColor = [UIColor colorWithWhite:1 alpha:1];
    menuButton.layer.cornerRadius = 5;
    menuButton.layer.masksToBounds= YES;
    [menuButton setTitle:@"Menu" forState:UIControlStateNormal];
    [menuButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    [menuButton addTarget:self action:@selector(menuTouch) forControlEvents:UIControlEventTouchDown];
    
    //Set up score label
    scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, screenWidth*0.40, screenHeight*0.05)];
    scoreLabel.backgroundColor = [UIColor colorWithWhite:1 alpha:1];
    scoreLabel.layer.cornerRadius = 5;
    scoreLabel.layer.masksToBounds = YES;
    scoreLabel.textColor = [UIColor blackColor];
    scoreLabel.textAlignment = NSTextAlignmentCenter;
    scoreLabel.text = [NSString stringWithFormat:@"Score: %d", score];
    
    
    //Set up hit zone
    hitZone = [[UIView alloc] initWithFrame:CGRectMake(-2, screenHeight*0.7+2, screenWidth+4, screenHeight*0.05)];
    hitZone.backgroundColor = [UIColor colorWithWhite:1 alpha:0.7];
    hitZone.layer.borderColor = [UIColor blackColor].CGColor;
    hitZone.layer.borderWidth = 2.0f;
    
    //Set up note dividers
    UIView* divider1 = [[UIView alloc] initWithFrame:CGRectMake(screenWidth*0.25-1, 0, 2, screenHeight)];
    divider1.backgroundColor = [UIColor blackColor];
    UIView* divider2 = [[UIView alloc] initWithFrame:CGRectMake(screenWidth*0.5-1, 0, 2, screenHeight)];
    divider2.backgroundColor = [UIColor blackColor];
    UIView* divider3 = [[UIView alloc] initWithFrame:CGRectMake(screenWidth*0.75-1, 0, 2, screenHeight)];
    divider3.backgroundColor = [UIColor blackColor];
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight*0.85, screenWidth, 2)];
    line.backgroundColor =[UIColor blackColor];
    
    //Set up display link
    display = [CADisplayLink displayLinkWithTarget:self selector:@selector(playGame)];
    display.preferredFramesPerSecond = 57;
    [display addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    
    //Set up the notes song
    for(int y=0; y<10; y++) {
        for(int x=0; x<4; x++) {
            notes[y][x] = [[UIView alloc] initWithFrame:CGRectMake(screenWidth*0.25*x, -screenHeight*0.05, screenWidth*0.25, screenHeight*0.05)];
            if(x == 0) {
                notes[y][x].backgroundColor = [UIColor redColor];
            } else if(x == 1) {
                notes[y][x].backgroundColor = [UIColor colorWithRed:244/255.0 green:221/255.0 blue:44/255.0 alpha:1];
            } else if(x == 2) {
                notes[y][x].backgroundColor = [UIColor blueColor];
            } else if(x == 3) {
                notes[y][x].backgroundColor = [UIColor colorWithRed:51/255.0 green:204/255.0 blue:71/255.0 alpha:1];
            }
            [notes[y][x] setHidden:YES];
            [self.view addSubview:notes[y][x]];
        }
    }
    
    //Set up menu
    int menuWidth = screenWidth*0.84;
    int menuHeight = screenHeight*0.84;
    menuShade = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    menuShade.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
    UIView* menu = [[UIView alloc] initWithFrame:CGRectMake(screenWidth*0.08, screenHeight*0.08, menuWidth, menuHeight)];
    menu.backgroundColor = [UIColor whiteColor];
    menu.layer.cornerRadius = 15;
    menu.layer.masksToBounds = YES;
    [menuShade addSubview:menu];
    
    UIColor* navy = [UIColor colorWithRed:1/255.0 green:48/255.0 blue:168/255.0 alpha:1];
    
    //Menu title
    UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake(menuWidth*0.05, menuHeight*0.05, menuWidth*0.90, menuHeight*0.10)];
    title.text = @"DRUM HERO";
    title.font = [UIFont fontWithName: @"Optima-ExtraBlack" size:title.frame.size.height/2];
    title.textAlignment = NSTextAlignmentCenter;
    title.textColor = navy;
    
    //Free mode button
    UIButton* freeMode = [[UIButton alloc] initWithFrame:CGRectMake(menuWidth*0.05, menuHeight*0.20, menuWidth*0.90, menuHeight*0.10)];
    freeMode.layer.borderColor = navy.CGColor;
    freeMode.layer.borderWidth = 2;
    [freeMode setTitle:@"Free Play" forState:UIControlStateNormal];
    [freeMode setTitleColor:navy forState:UIControlStateNormal];
    [freeMode addTarget:self action:@selector(playFreeMode) forControlEvents:UIControlEventTouchUpInside];
    
    //Random note mode button
    UIButton* randNotes = [[UIButton alloc] initWithFrame:CGRectMake(menuWidth*0.05, menuHeight*0.35, menuWidth*0.90, menuHeight*0.10)];
    randNotes.layer.borderColor = navy.CGColor;
    randNotes.layer.borderWidth = 2;
    [randNotes setTitle:@"Random Play" forState:UIControlStateNormal];
    [randNotes setTitleColor:navy forState:UIControlStateNormal];
    [randNotes addTarget:self action:@selector(playRandNotes) forControlEvents:UIControlEventTouchUpInside];
    
    //Song mode button
    UIButton* songPlay = [[UIButton alloc] initWithFrame:CGRectMake(menuWidth*0.05, menuHeight*0.50, menuWidth*0.90, menuHeight*0.10)];
    songPlay.layer.borderColor = navy.CGColor;
    songPlay.layer.borderWidth = 2;
    [songPlay setTitle:@"Song Play" forState:UIControlStateNormal];
    [songPlay setTitleColor:navy forState:UIControlStateNormal];
    [songPlay addTarget:self action:@selector(playSongPlay) forControlEvents:UIControlEventTouchUpInside];
    
    //Difficulty label
    UILabel* difficulty = [[UILabel alloc] initWithFrame:CGRectMake(menuWidth*0.05, menuHeight*0.62, menuWidth*0.90, menuHeight*0.10)];
    difficulty.text = @"Choose Difficulty";
    difficulty.font = [UIFont fontWithName:@"Optima-ExtraBlack" size:difficulty.frame.size.height/2];
    difficulty.textAlignment = NSTextAlignmentCenter;
    difficulty.textColor = navy;
    
    //Difficulty selector
    levels = [[UISegmentedControl alloc] initWithFrame:CGRectMake(menuWidth*0.05, menuHeight*0.74, menuWidth*0.90, menuHeight*0.10)];
    levels.tintColor = navy;
    [levels insertSegmentWithTitle:@"Easy" atIndex:0 animated:NO];
    [levels insertSegmentWithTitle:@"Medium" atIndex:1 animated:NO];
    [levels insertSegmentWithTitle:@"Hard" atIndex:2 animated:NO];
    levels.selectedSegmentIndex = 0;
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"Optima-ExtraBlack" size:levels.frame.size.height/3] forKey:NSFontAttributeName];
    [levels setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    //Back button
    UIButton* back = [[UIButton alloc] initWithFrame:CGRectMake(menuWidth*0.30, menuHeight*0.90, menuWidth*0.40, menuHeight*0.05)];
    back.layer.borderColor = navy.CGColor;
    back.layer.borderWidth = 2;
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setTitleColor:navy forState:UIControlStateNormal];
    [back addTarget:self action:@selector(menuBack) forControlEvents:UIControlEventTouchUpInside];
    
    //Add items to menu
    [menu addSubview:title];
    [menu addSubview:freeMode];
    [menu addSubview:randNotes];
    [menu addSubview:songPlay];
    [menu addSubview:difficulty];
    [menu addSubview:levels];
    [menu addSubview:back];
    
    //Set up end of game window
    int gameOverWidth = screenWidth*0.8;
    int gameOverHeight = screenHeight*0.5;
    gameOver = [[UIView alloc] initWithFrame:CGRectMake(screenWidth*0.1, screenHeight*0.2, gameOverWidth, gameOverHeight)];
    gameOver.backgroundColor = [UIColor colorWithWhite:1 alpha:1];
    gameOver.layer.cornerRadius = 15;
    gameOver.layer.masksToBounds = YES;
    
    //Game over title label
    gameOverLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, gameOverHeight*0.1, gameOverWidth, gameOverHeight*0.3)];
    gameOverLabel.text = @"Game Over";
    gameOverLabel.textColor = navy;
    gameOverLabel.font = [UIFont fontWithName:@"Optima-ExtraBlack" size:gameOverLabel.frame.size.height/2];
    gameOverLabel.textAlignment = NSTextAlignmentCenter;
    
    //Note streak label
    streakLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, gameOverHeight*0.4, gameOverWidth, gameOverHeight*0.2)];
    streakLabel.text = [NSString stringWithFormat:@"Longest streak: %d notes", longestStreak];
    streakLabel.textColor = navy;
    streakLabel.font = [UIFont fontWithName:@"Optima-ExtraBlack" size:streakLabel.frame.size.height/3];
    streakLabel.textAlignment = NSTextAlignmentCenter;
    
    //Another menu button
    UIButton* gameOverButton = [[UIButton alloc] initWithFrame:CGRectMake(gameOverWidth*0.1, gameOverHeight*0.7, gameOverWidth*0.8, gameOverHeight*0.2)];
    [gameOverButton setTitle:@"New Game" forState:UIControlStateNormal];
    [gameOverButton setTitleColor:navy forState:UIControlStateNormal];
    gameOverButton.layer.borderColor = navy.CGColor;
    gameOverButton.layer.borderWidth = 2;
    [gameOverButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    
    //Add items to game over view
    [gameOver addSubview:gameOverLabel];
    [gameOver addSubview:streakLabel];
    [gameOver addSubview:gameOverButton];
    [gameOver setHidden:YES];
    
    //Set up countdown label
    countdown = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.20, screenHeight*0.10, screenWidth*0.60, screenHeight*0.60)];
    countdown.textColor = navy;
    countdown.textAlignment = NSTextAlignmentCenter;
    countdown.font = [UIFont fontWithName:@"Optima-ExtraBlack" size:countdown.frame.size.height-50];
    [countdown setHidden:YES];
    
    //Set up sounds
    NSString *path = [NSString stringWithFormat:@"%@/snare.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    snareSound = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    path = [NSString stringWithFormat:@"%@/hihat.wav", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    hihatSound = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    path = [NSString stringWithFormat:@"%@/tomtom.wav", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    tomtomSound = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    path = [NSString stringWithFormat:@"%@/cymbal.mp3", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    cymbalSound = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    path = [NSString stringWithFormat:@"%@/Rage Against The Machine - Bulls On Parade.mp3", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    bulls = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    //Add subviews to screen
    [self.view addSubview:snare];
    [self.view addSubview:hihat];
    [self.view addSubview:tomtom];
    [self.view addSubview:cymbal];
    [self.view addSubview:hitZone];
    [self.view addSubview:divider1];
    [self.view addSubview:divider2];
    [self.view addSubview:divider3];
    [self.view addSubview:line];
    [self.view addSubview:countdown];
    [self.view addSubview:menuButton];
    [self.view addSubview:scoreLabel];
    [self.view addSubview:menuShade];
    [self.view addSubview:gameOver];
}

//Update red shade
-(void)shadeRed {
    float shade = 0;
    if(safe < 15) {
        shade = (1 - (float)safe/15)*0.6;
        if(shade < 0) shade = 0;
        redShade.backgroundColor = [UIColor colorWithRed:192/255.0 green:0 blue:0 alpha:shade];
    } else if(safe > 25) {
        shade = (1 - (float)(40-safe)/15)*0.5;
        if(shade < 0) shade = 0;
        redShade.backgroundColor = [UIColor colorWithRed:1 green:242/255.0 blue:0 alpha:shade];
    }
}

//Initialize values
-(void)initGame {
    score = 0;
    longestStreak = 0;
    streak = 0;
    safe = 20;
    counter = -180;
    idx = 0;
    song = intro;
    intro[0] = 2;
    redShade.backgroundColor = [UIColor colorWithRed:1.0 green:0 blue:0 alpha:0];
}

//Make a note appear at top of screen at given drum index
-(void)spawnNote:(int)drum {
    for(int y=0; y<10; y++) {
        if(notes[y][drum].isHidden) {
            [notes[y][drum] setHidden:NO];
            notes[y][drum].frame = CGRectMake(screenWidth*0.25*drum, -screenHeight*0.05, screenWidth*0.25, screenHeight*0.05);
            y = 10;
        }
    }
}

//Move notes downward at appropriate speed
-(void)moveNotes {
    for(int y=0; y<10; y++) {
        for(int x=0; x<4; x++) {
            if(!notes[y][x].isHidden) {
                notes[y][x].center = CGPointMake(notes[y][x].center.x, notes[y][x].center.y+noteSpeed);
            }
            if(notes[y][x].frame.origin.y > screenHeight*0.85) {
                if(! notes[y][x].isHidden) {
                    if(safe > 0) {
                        streak = 0;
                        safe--;
                        if(safe == 0) {
                            [gameOver setHidden:NO];
                            gameOverLabel.text = @"You Failed!";
                        }
                    }
                }
                [notes[y][x] setHidden:YES];
            }
        }
    }
    [self shadeRed];
}

//Main loop
- (void)playGame {
    //Only display notes for random play and song play
    if(menuShade.isHidden && gameOver.isHidden && mode > 0) {
        //Update streak label in game over window
        streakLabel.text = [NSString stringWithFormat:@"Longest streak: %d notes", longestStreak];
        //Update score label
        scoreLabel.text = [NSString stringWithFormat:@"Score: %d", score];
        //scoreLabel.text = [NSString stringWithFormat:@"Counter: %ld", counter/10];
        //Countdown from 3 at beginning of game
        if(counter < 0 && counter%60 == 0) {
            countdown.text = [NSString stringWithFormat:@"%ld",-counter/60];
            if(counter == -180) [tomtomSound play];
            else if(counter == -120) [hihatSound play];
            else if(counter == -60) [snareSound play];
            [countdown setHidden:NO];
        //Remove countdown after game has started
        } else if(counter > 0) {
            [countdown setHidden:YES];
        } else if(counter == 0) [cymbalSound play];
        //Random play mode
        if(mode == 1) {
            [self moveNotes];
            //If countdown label is gone and less than 2 minutes have elapsed, spawn a random note at level-appropriate rate
            if(counter > 0 && counter%(60/(level+2)) == 0 && counter < 6840) {
                int randDrum = rand()%4;
                [self spawnNote:randDrum];
            }
        //Song play mode
        } else if(mode == 2) {
            [self moveNotes];
            if(counter < 200) {
                if(!notes[0][0].isHidden && notes[0][0].frame.origin.y >= 1.7*screenHeight/noteSpeed) [bulls play];
            }
            if(counter > 0 && counter%10 == 0) { //((counter < 2700 && counter%10 == 0) || (counter > 2700 && counter%11 == 0)) ) {
                if(song[idx]%2 == 0) {
                    [self spawnNote:0];
                }
                if(song[idx]%3 == 0) {
                    [self spawnNote:1];
                }
                if(song[idx]%5 == 0) {
                    [self spawnNote:2];
                }
                if(song[idx]%7 == 0) {
                    [self spawnNote:3];
                }
                idx++;
                if(idx == 67 && counter < 1300) {
                    idx = 0;
                    song[idx] = 1;
                } else if(counter > 1370 && counter < 2050) idx = 0;
                else if(counter == 2050) {
                    idx = 0;
                    song = bridge;
                } else if(idx == 17 && counter > 2050 && counter < 2500) idx = 0;
                else if(idx == 17 && counter > 2500 && counter < 2600) {
                    song = verse;
                    idx = 0;
                } else if(idx == 44 && counter > 2600) idx = 10;
                else if(counter > 4060) idx = 1;
            }
        }
        counter++;
        if((mode == 1 && counter == 7000) || (mode == 2 && counter == 4400)) {
            [gameOver setHidden:NO];
            gameOverLabel.text = @"Success!";
        }
    } else if(!gameOver.isHidden && [gameOverLabel.text isEqualToString:@"You Failed!"] && mode == 2) [bulls stop];
}

//Remove notes from playing screen
- (void)clearNotes {
    for(int y=0; y<10; y++) {
        for(int x=0; x<4; x++) {
            [notes[y][x] setHidden:YES];
        }
    }
}

//Set up free mode
- (void)playFreeMode {
    [menuShade setHidden:YES];
    [scoreLabel setHidden:YES];
    mode = 0;
    [self clearNotes];
    [countdown setHidden:YES];
    [self initGame];
}

//Set up random notes play
- (void)playRandNotes {
    [menuShade setHidden:YES];
    [scoreLabel setHidden:NO];
    mode = 1;
    level = (int)levels.selectedSegmentIndex;
    noteSpeed = ((level+1)*0.005)*screenHeight;
    [self clearNotes];
    [countdown setHidden:NO];
    [self initGame];

}

//Set up song play
- (void)playSongPlay {
    [menuShade setHidden:YES];
    [scoreLabel setHidden:NO];
    mode = 2;
    level = (int)levels.selectedSegmentIndex;
    noteSpeed = ((level+1)*0.005)*screenHeight;
    [self clearNotes];
    [countdown setHidden:NO];
    [self initGame];
    bulls.currentTime = 0;

}

//Check for hit with given drum index and update score, streak, safe
- (bool)checkHit:(int)drum {
    bool hit = NO;
    for(int i=0; i<10; i++) {
        if(CGRectIntersectsRect(notes[i][drum].frame, hitZone.frame) && ! notes[i][drum].isHidden) {
            hit = YES;
            [notes[i][drum] setHidden:YES];
            streak++; //Increment current streak
            if(streak > longestStreak) longestStreak = streak; //Update longest streak
            if(safe < 30) safe++; //Increment safe value if below max value (30)
            int multiplier = streak/10 + 1;
            if(streak >= 40) multiplier = 4;
            score += 100 * multiplier; //Update score according to current streak
        }
    }
    if(!hit) {
        streak = 0;
        if(safe > 0) {
            safe--;
            if(safe == 0) {
                [gameOver setHidden:NO];
                gameOverLabel.text = @"You Failed!";
            }
        }
    }
    [self shadeRed];
    return hit;
}

//Change red button color, check for red note hit
- (void)snareHit {
    snare.backgroundColor = [UIColor colorWithRed:255/255.0 green:112/255.0 blue:112/255.0 alpha:1];
    bool hit = YES;
    if(mode > 0) hit = [self checkHit: 0];
    if(mode < 2 && hit) {
        snareSound.currentTime = 0;
        [snareSound play];
    }
}

//Change yellow button color, check for yellow note hit
- (void)hihatHit {
    hihat.backgroundColor = [UIColor colorWithRed:245/255.0 green:247/255.0 blue:108/255.0 alpha:1];
    bool hit = YES;
    if(mode > 0) hit = [self checkHit: 1];
    if(mode < 2 && hit) {
        hihatSound.currentTime = 0;
        [hihatSound play];
    }
}

//Change blue button color, check for blue note hit
- (void)tomtomHit {
    tomtom.backgroundColor = [UIColor colorWithRed:70/255.0 green:146/255.0 blue:252/255.0 alpha:1];
    bool hit = YES;
    if(mode > 0) hit = [self checkHit: 2];
    if(mode < 2 && hit) {
        tomtomSound.currentTime = 0;
        [tomtomSound play];
    }
}

//Change green button color, check for green note hit
- (void)cymbalHit {
    cymbal.backgroundColor = [UIColor colorWithRed:121/255.0 green:247/255.0 blue:138/255.0 alpha:1];
    bool hit = YES;
    if(mode > 0) hit = [self checkHit: 3];
    if(mode < 2 && hit) {
        cymbalSound.currentTime = 0;
        [cymbalSound play];
    }
}

//Make menu appear
- (void)showMenu {
    [menuShade setHidden:NO];
    menuButton.backgroundColor = [UIColor colorWithWhite:1 alpha:1];
    [gameOver setHidden:YES];
    if(mode == 2) [bulls stop];
}

//Make menu disappear
- (void)menuBack {
    [menuShade setHidden:YES];
    if(mode == 2) [bulls play];
}

//Button functions to return each button back to its original color when touched up inside and outside
- (void)snareRelease1 {
    snare.backgroundColor = [UIColor redColor];
}

- (void)snareRelease2 {
    snare.backgroundColor = [UIColor redColor];
}

- (void)hihatRelease1 {
    hihat.backgroundColor = [UIColor colorWithRed:244/255.0 green:221/255.0 blue:44/255.0 alpha:1];
}

- (void)hihatRelease2 {
    hihat.backgroundColor = [UIColor colorWithRed:244/255.0 green:221/255.0 blue:44/255.0 alpha:1];
}

- (void)tomtomRelease1 {
    tomtom.backgroundColor = [UIColor blueColor];
}

- (void)tomtomRelease2 {
    tomtom.backgroundColor = [UIColor blueColor];
}

- (void)cymbalRelease1 {
    cymbal.backgroundColor = [UIColor colorWithRed:51/255.0 green:204/255.0 blue:71/255.0 alpha:1];
}

- (void)cymbalRelease2 {
    cymbal.backgroundColor = [UIColor colorWithRed:51/255.0 green:204/255.0 blue:71/255.0 alpha:1];
}

- (void)menuTouch {
    menuButton.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
